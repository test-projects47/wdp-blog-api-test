import styles from '../styles/Home.module.css'
import BlogPostCard from '../components/BlogPostCard';


const DevTo = ({posts}) => {
    return (
        <main className={styles.container}>
            <h1>
                Dev.to Test Organisation Blog
            </h1>
            <p>
                displaying dev.to blog posts using the Dev.to API
            </p>
            <div className={styles.grid}>
                {posts.map((post) => (
                    <BlogPostCard post={post} key={post.id}/>
                ))}
            </div>
        </main>)
}

export async function getStaticProps(){
    const res = await fetch('https://dev.to/api/articles?username=wdp')
    const posts = await res.json()

    return{
        props:{
            posts
        },
      }
}

export default DevTo