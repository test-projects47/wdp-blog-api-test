import Link from 'next/link';
import styles from '../styles/Home.module.css'

const BlogPostCard = ({post}) => {
    return(
        <div className={styles.card}>
            <Link href={`posts/${post.slug}`}>{post.title}</Link>
        </div>
    )
 }

 export default BlogPostCard